module.exports = {
    plugins: [
        require('@poi/plugin-eslint')({
        }),
    ],

    webpack(config, context) {
        config.output.publicPath = '';

        config.output.filename = 'customize-widget.js';
        config.output.library = 'CustomizeWidget';

        return config;
    },
};
