const poi = require('poi')
const config = require('./poi.config')

const app = new poi(config)
const webpackConfig = app.createWebpackConfig()

module.exports = {
    root: true,
    extends: 'airbnb-base',
    plugins: [
        'vue',
        'html',
        'import'
    ],
    env: {
        browser: true,
    },
    'rules': {
        indent: ['error', 4, {
            SwitchCase: 1,
            VariableDeclarator: 1,
            outerIIFEBody: 1,
            MemberExpression: 0,
            FunctionDeclaration: {
                parameters: 1,
                body: 1
            },
            FunctionExpression: {
                parameters: 1,
                body: 1
            },
            CallExpression: {
                arguments: 1
            },
            ArrayExpression: 1,
            ObjectExpression: 1,
            ImportDeclaration: 1,
            flatTernaryExpressions: false,
            ignoredNodes: ['JSXElement', 'JSXElement *']
        }],
        "no-param-reassign": [2, {
            "props": false
        }],
        "prefer-template": 0,
        "max-len": ["error", 160],
        "comma-dangle": ['error', {
            arrays: 'always-multiline',
            objects: 'always-multiline',
            imports: 'always-multiline',
            exports: 'always-multiline',
            functions: 'ignore',
        }],
        "function-paren-newline": ["error", "consistent"],
        "import/no-extraneous-dependencies": 0,
        // don't require .vue extension when importing
        'import/extensions': ['error', 'always', {
            'js': 'never',
            'vue': 'never'
        }],
        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
        'linebreak-style': 0,
    },
    settings: {
        'import/resolver': {
            webpack: {
                config: webpackConfig
            },
            node: {
                extensions: ['.js', '.jsx', '.vue']
            }
        },
    },
};
