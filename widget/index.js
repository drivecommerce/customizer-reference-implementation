import Vue from 'vue';

import CustomizerView from './components/Customizer';

// Main entry point to the widget.
function createWidget(options) {
    let instance = null;

    // Load the client library.
    (function ClientLoader(url, onload) {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.onload = onload;
        script.src = url;
        document.head.appendChild(script);
    }('https://api.customizer.drivecommerce.com/api/v2/runtime/client?type=production', () => {
        // Create the Vue app instance using provided options.
        instance = new Vue({
            el: options.container,
            render: h => h(CustomizerView, {
                props: {
                    options,
                },
            }),
        });
    }));

    function destroy() {
        if (instance) {
            instance.$destroy();
        }
    }

    return {
        destroy,

        on: {
        },
    };
}

// Expose the widget library namespace.
function createNamespace() {
    return {
        createWidget,
    };
}

export default createNamespace();
